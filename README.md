# postmarketOS AppArmor profiles

Naming convention: profiles must have the same name, as the Alpine /
postmarketOS package. This is needed to keep packaging simple.

Related:
* [alpine-devel post](https://lists.alpinelinux.org/~alpine/devel/%3C12b0185e-f821-4a7d-7164-fd0398db0739%40postmarketos.org%3E)
* [pma!2624](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2624)
